def char_types_count(ulaz):
    uppercase_count = 0
    lowercase_count = 0
    number_count = 0

    for char in ulaz:
        if char.isupper():
            uppercase_count += 1
        elif char.islower():
            lowercase_count += 1
        elif char.isdigit():
            number_count += 1

    return (uppercase_count, lowercase_count, number_count)

ulaz = input("Unesite string: ")
print(char_types_count(ulaz))