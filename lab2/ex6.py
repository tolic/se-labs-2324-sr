import json

class Employee:

    def __init__(self, name, title, age, office):
        self.name = name
        self.title = title
        self.age = age
        self.office = office

    def __str__(self):
        return f"{self.name} ({self.age}), {self.title} @ {self.office}"


import json

class Company:

    def __init__(self, name):
        self.name = name
        self.employees = []

    def __str__(self):
        return f"{self.name} ({len(self.employees)} employees)"

    def employ(self, name, title, age, office):
        new_employee = Employee(name, title, age, office)
        self.employees.append(new_employee)

    def fire(self, name):
        for e in self.employees:
            if e.name == name:
                self.employees.remove(e)

    def load_from_json(self, path_to_json):
        try:
            with open(path_to_json, 'r') as json_file:
                data = json.load(json_file)
                
                if 'name' in data:
                    self.name = data['name']
                
                if 'employees' in data and isinstance(data['employees'], list):
                    self.employees = [Employee(e['name'], e['title'], e['age'], e['office']) for e in data['employees']]
                else:
                    self.employees = []
        except (IOError, json.JSONDecodeError):
            print("Error loading data from the JSON file.")

    def save_to_json(self, path_to_json):
        data = {
            'name': self.name,
            'employees': [{'name': e.name, 'title': e.title, 'age': e.age, 'office': e.office} for e in self.employees]
        }
        with open(path_to_json, 'w') as json_file:
            json.dump(data, json_file, indent=4)

    def print_employees(self):
        print(f"{self.name}\n{'-' * len(self.name)}")
        for i, employee in enumerate(self.employees, 1):
            print(f"{i}. {str(employee)}")

def main():
    nike = Company("Nike")
    print(nike)

    nike.employ("Homer Simpson", "CEO", 44, "Lobby")
    nike.employ("Marge Simpson", "CTO", 33, "Lobby")
    print(nike)

    nike.fire("Homer Simpson")
    print(nike)

    adidas = Company("Adidas")
    adidas.load_from_json("C:\\student\\lv2tolic\\se-labs-2324-sr\\lab2\\ex4-employees.json")
    print(adidas)
    adidas.print_employees()

    adidas.employ("Homer Simpson", "CEO", 44, "Lobby")
    adidas.employ("Marge Simpson", "CTO", 33, "Lobby")
    adidas.employ("Bart Simpson", "CEO", 44, "Lobby")
    adidas.employ("Lisa Simpson", "CTO", 33, "Lobby")
    print(adidas)
    adidas.print_employees()

    adidas.fire("Homer Simpson")
    adidas.fire("Marge Simpson")
    print(adidas)
    adidas.print_employees()

    adidas.save_to_json("C:\student\lv2tolic\se-labs-2324-sr\lab2\ex6-employees.json")

if __name__ == "__main__":
    main()
