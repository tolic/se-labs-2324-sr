file = 'C:\student\lv2tolic\se-labs-2324-sr\lab2\ex2-text.csv'
employees_file_path = 'C:\student\lv2tolic\se-labs-2324-sr\lab2\ex2-employees.txt'
locations_file_path = 'C:\student\lv2tolic\se-labs-2324-sr\lab2\ex2-locations.txt'
employees = []
locations = []


with open(file, 'r') as file:
    for line in file:
        data = line.strip().split(',')
        if len(data) >= 2:
            employee = data[0].strip()
            job_title = data[1].strip()
            office = data[3].strip()
            employees.append(f"{employee},{job_title}")
            locations.append(f"{employee},{office}")


with open(employees_file_path, 'w') as employees_file:
    employees_file.write("\n".join(employees))


with open(locations_file_path, 'w') as locations_file:
    locations_file.write("\n".join(locations))
