from query_handler_base import QueryHandlerBase
import random
import requests
import json

class DadJokeHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "daddy" in query:
            return True
        return False

    def process(self, query):
        try:
            result = self.call_api()
            txt = result["joke"]
            self.ui.say(txt)
        except Exception as e: 
            self.ui.say("Oh no! There was an error.")
            self.ui.say("Try something else!")



    def call_api(self):
        url = "https://daddyjokes.p.rapidapi.com/random"

        headers = {
                "X-RapidAPI-Key": "0be0f3fa10mshafdf9d426f39379p1923f3jsn87d1d2a3efb8",
	            "X-RapidAPI-Host": "daddyjokes.p.rapidapi.com"
        }

        response =  requests.get(url, headers=headers)

        return json.loads(response.text)



