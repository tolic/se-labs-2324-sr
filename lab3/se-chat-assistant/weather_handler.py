from query_handler_base import QueryHandlerBase
import random
import requests
import json

class WeatherHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "weather" in query:
            return True
        return False

    def process(self, query):
        input = query.split()
        city=input[1]

        try:
            result = self.call_api(city)
            txt = result["current"]["temp_c"]
            self.ui.say(txt)
        except Exception as e: 
            self.ui.say("Something went wrong")
            



    def call_api(self, city):
        url = "https://weatherapi-com.p.rapidapi.com/current.json"

        querystring = {"q":city}

        headers = {
                "X-RapidAPI-Key": "0be0f3fa10mshafdf9d426f39379p1923f3jsn87d1d2a3efb8",
	            "X-RapidAPI-Host": "weatherapi-com.p.rapidapi.com"

        }

        response = requests.get(url, headers=headers, params=querystring)

        return json.loads(response.text)



